package externalMergeSort;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExternalMergeSort {
	
	static final Long LINES_IN_MEMORY = 70000l;
	
	public static void main(String[] args) throws IOException {
		
		String encoding = "ISO-8859-1";
		
		FileInputStream fileInputStream = new FileInputStream("CEP.txt");
		InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, encoding);
		BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
		
		String currentLine = bufferedReader.readLine();
		
		Integer totalLines = 0;
		
		int numberOfFiles = 0;
		//Divide os arquivos
		do {			
			PrintWriter printWriter = new PrintWriter(numberOfFiles + ".txt", encoding);
			BufferedWriter bufferedWriter = new BufferedWriter(printWriter);
			
			int counter = 0;

			List<Line> linesInMemory = new ArrayList<>();
			while(currentLine != null && counter < LINES_IN_MEMORY)
			{
				linesInMemory.add(new Line(currentLine));
				currentLine = bufferedReader.readLine();
				totalLines++;
				counter++;
			}
			
			Collections.sort(linesInMemory);
			
			for (Line lineCounter : linesInMemory) {
				bufferedWriter.write(lineCounter.toString());
				bufferedWriter.newLine();
			}

			bufferedWriter.close();
			printWriter.close();
			numberOfFiles++;
		} while (currentLine != null);

		bufferedReader.close();
		inputStreamReader.close();
		fileInputStream.close();
		
		FileInputStream[] fileInputStreamArray = new FileInputStream[numberOfFiles];
		InputStreamReader[] inputStreamReaderArray = new InputStreamReader[numberOfFiles];
		BufferedReader[] bufferedReaderArray = new BufferedReader[numberOfFiles];
		
		//List<Line> firstCepList = new ArrayList<>();
		Line[] firstCepList  = new Line[numberOfFiles];
		
		for (int counter = 0; counter < numberOfFiles; counter++) {
			fileInputStreamArray[counter] = new FileInputStream(counter + ".txt");
			inputStreamReaderArray[counter] = new InputStreamReader(fileInputStreamArray[counter], encoding);
			bufferedReaderArray[counter] = new BufferedReader(inputStreamReaderArray[counter]);
			firstCepList[counter] = new Line(bufferedReaderArray[counter].readLine());
		}
		
		PrintWriter printWriter = new PrintWriter("sorted.txt", encoding);
		
		for (int totalLinesCounter = 0; totalLinesCounter < totalLines; totalLinesCounter++) {
			Line smallerCep = firstCepList[0];
			int smallerFile = 0;
			for (int fileCounter = 0; fileCounter < numberOfFiles; fileCounter++) {
				if (smallerCep.compareTo(firstCepList[fileCounter]) > 0) {
					smallerCep = firstCepList[fileCounter];
					smallerFile = fileCounter;
				}
			}
			
			printWriter.println(smallerCep.toString());
			
			String nextLine = bufferedReaderArray[smallerFile].readLine();
			if (nextLine != null) {
				firstCepList[smallerFile] = new Line(nextLine);	
			}
			else{
				String cep = "                                                                                                                                                                                                                                                                                                  99999999 ";
				firstCepList[smallerFile] = new Line(cep);
			}
		}
		
		printWriter.close();
		
		for (int counter = 0; counter < numberOfFiles; counter++) {
			bufferedReaderArray[counter].close();
			inputStreamReaderArray[counter].close();
			fileInputStreamArray[counter].close();
		}
		
		System.out.println("Veja o arquivo sorted.txt com a saida do algoritmo.");
	}
	
}